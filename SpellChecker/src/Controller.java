import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.languagetool.JLanguageTool;
import org.languagetool.language.German;
import org.languagetool.rules.RuleMatch;


public class Controller {
	final static Logger logger = LogManager.getLogger(Controller.class);
	
	private PostgreCon con;
	private Map<String, Integer> _map_title_fehler;
	
	
	public Controller() {
		
		con = new PostgreCon();	
		_map_title_fehler = new HashMap<>();
	}

	public static void main(String[] args) throws IOException {		
		Controller ctrl = new Controller();
		ctrl.getTitle();
	}

	private void getTitle() {
		List<String> titleList = con.getTitle();
		LangTool lt = new LangTool(titleList);
		Thread th = new Thread(lt);
		th.start();
		try {
			th.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		_map_title_fehler = lt.get_map_title_fehler();
		
		// Sortiere und gebe aus.
		TreeMap<String, Integer> sort = new TreeMap<>(new ValueComparator(_map_title_fehler));
		sort.putAll(_map_title_fehler);
		
		for(String key : sort.keySet()){
			System.out.println(key+ ": "+ _map_title_fehler.get(key));
		}
	}
}
