import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class PostgreCon {
	final static Logger logger = LogManager.getLogger(PostgreCon.class);
	private Connection connection;
	
	private final String url = "jheym.de:5432/datatextmining";
	private final String user ="postgres";
	private final String pswd ="iHC3QZ2zKQx1b2QW21hq";
	
	// ##################### KONSTRUKTOREN #################################
	
	public PostgreCon(){
		//Verbindung zur DB aufbauen
		try {
			connection = DriverManager.getConnection("jdbc:postgresql://"+url, user, pswd);
			connection.setAutoCommit(false); //AutoCommit deaktivieren
		} catch (SQLException e) {
			logger.fatal("Verbindung zur Datenbank fehlgeschlagen!");
		}
	}
	
	// #####################################################################

	public void commit(){
		try {
			connection.commit();
		} catch (SQLException e) {
			logger.error("commit: "+e.getMessage());
		}
	}
	
	public void rollback(){
		try {
			connection.rollback();
		} catch (SQLException e) {
			logger.error("rollback: "+e.getMessage());
		}
	}

	public List<String> getTitle(){
		List<String> titleList = new ArrayList<>();
		try {
			PreparedStatement st = connection.prepareStatement("SELECT d.title daten_title "
														+ "FROM daten d"
//														+ "WHERE t.id= ?"
														);
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				titleList.add(rs.getString("daten_title"));
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return titleList;
	}
	
	
}
