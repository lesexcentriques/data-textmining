package model;

public class Review {
	private int id;
	private String title;
	private String text;
	private int rating;
	private boolean verified;
	private int counterHelpful;
	private int counterAll;
	private String author;
	private String date;
	private int itemId;
	private String category;
	
	public Review(int id, String title, String text, int rating,
			boolean verified, int counterHelpful, int counterAll,
			String author, String date, int itemId, String category) {
		super();
		this.id = id;
		this.title = title;
		this.text = text;
		this.rating = rating;
		this.verified = verified;
		this.counterHelpful = counterHelpful;
		this.counterAll = counterAll;
		this.author = author;
		this.date = date;
		this.itemId = itemId;
		this.category = category;
	}
	public int getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public String getText() {
		return text;
	}
	public int getRating() {
		return rating;
	}
	public boolean isVerified() {
		return verified;
	}
	public int getCounterHelpful() {
		return counterHelpful;
	}
	public int getCounterAll() {
		return counterAll;
	}
	public String getAuthor() {
		return author;
	}
	public String getDate() {
		return date;
	}
	public int getItemId() {
		return itemId;
	}
	public String getCategory() {
		return category;
	}
	@Override
	public String toString() {
		return "Review [id=" + id + ", title=" + title + ", text=" + text
				+ ", rating=" + rating + ", verified=" + verified
				+ ", counterHelpful=" + counterHelpful + ", counterAll="
				+ counterAll + ", author=" + author + ", date=" + date
				+ ", itemId=" + itemId + ", category=" + category + "]";
	}
	
	
}
