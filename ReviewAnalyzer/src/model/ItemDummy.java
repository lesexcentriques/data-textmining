package model;

import java.util.ArrayList;

public class ItemDummy {
	private int id;
	private ArrayList<Review> reviews;
	
	private double avgRating;
	private double variance;
	
	public ItemDummy(int id, ArrayList<Review> reviews) {
		this.id = id;
		this.reviews = reviews;
	}

	public void setAvgRating(double avgRating) {
		this.avgRating = avgRating;
	}

	public void setVariance(double variance) {
		this.variance = variance;
	}
	
	public int getId() {
		return id;
	}

	public ArrayList<Review> getReviews() {
		return reviews;
	}

	public double getAvgRating() {
		return avgRating;
	}
	
	public double getVariance() {
		return variance;
	}

	public double getStandardDeviation() {
		return Math.sqrt(variance);
	}

	@Override
	public String toString() {
		return "ItemDummy [id=" + id + /*", reviews=" + reviews +*/ ", avgRating="
				+ avgRating + ", standardDeviation=" + this.getStandardDeviation() + "]";
	}
	
	
}
