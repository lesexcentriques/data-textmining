package model;

import java.util.ArrayList;

public class Author {
	private int id;
	private int authorId;
	private double avgrating;
	private int posts;
	private int points;
	
	// Für High-Low Bewertung
	private ArrayList<Review> reviews;
	private int cntOnePointReviews = 0;
	private int cntFivePointsReviews = 0; 
	
	public Author(int id, int authorId, double avgrating, int posts, int points) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.avgrating = avgrating;
		this.posts = posts;
		this.points = points;
	}
	
	public void setReviews(ArrayList<Review> reviews) {
		this.reviews = reviews;
	}
	public void incrementPoints(int points) {
		this.points += points;
	}
	public void incrementOnePointReviews() {
		++cntOnePointReviews;
	}
	public void incrementFivePointsReviews() {
		++cntFivePointsReviews;
	}
	
	
	public int getId() {
		return id;
	}
	public int getAuthorId() {
		return authorId;
	}
	public double getAvgrating() {
		return avgrating;
	}
	public int getPosts() {
		return posts;
	}
	public int getPoints() {
		return points;
	}
	public ArrayList<Review> getReviews() {
		return reviews;
	}
	public int getCntOnePointReviews() {
		return cntOnePointReviews;
	}
	public int getCntFivePointsReviews() {
		return cntFivePointsReviews;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", authorId=" + authorId + ", avgrating="
				+ avgrating + ", posts=" + posts + ", points=" + points
				/*+ ", reviews=" + reviews*/ + ", cntOnePointReviews="
				+ cntOnePointReviews + ", cntFivePointsReviews="
				+ cntFivePointsReviews + "]";
	}
	
	
	
}
