package ctrl;

import java.util.ArrayList;

import model.Author;
import model.Review;

/*
 * Fake-Reviews machen nur Sinn, wenn sie sehr positiv oder sehr negativ sind.
 * Wenn ein Nutzer nie 2-4 Sterne vergibt, ist das verdächtig.
 */

public class HighLowRating {
	private DbCtrl dbCtrl;
	private ArrayList<Author> authors;
	private int minReviews;
	
	public HighLowRating(DbCtrl dbCtrl, int minReviews) {
		this.dbCtrl = dbCtrl;
		this.minReviews = minReviews-1;
	}
	
	public void doIt() {
		authors = dbCtrl.getAuthors();
		
		for (Author author : authors) {
			if (author.getPosts() > minReviews) {
				this.countReviewsFromAuthor(author);
				this.calcPoints(author);
				System.out.println(author);
			}
		}
 	}
	
	private void countReviewsFromAuthor(Author author) {
		for (Review review : dbCtrl.getReviewsByAuthor(author.getAuthorId())) {
			switch (review.getRating()) {
			case 1:
				author.incrementOnePointReviews();
				break;
			case 5:
				author.incrementFivePointsReviews();
				break;
			default:
				break;
			}
		}
	}
	
	private void calcPoints(Author author) {
		double percentageOne = (1.0 / author.getPosts()) * author.getCntOnePointReviews();
		double percentageFive = (1.0 / author.getPosts()) * author.getCntFivePointsReviews();
		
		double a;
		double b = 1.0 - percentageOne - percentageFive;
		double statementOne = 0.8;
		double statementTwo = 2;
		
		// Variablen setzen
		if (percentageOne > percentageFive) {
			a = percentageOne;
		} else {
			a = percentageFive;
		}
		
		// Variablen für Berechnungen ermitteln
		if (author.getPosts() < 50) {
			statementOne = 0.8;
			statementTwo = 4;
		} else if(author.getPosts() < 100) {
			statementOne = 0.7;
			statementTwo = 4;
		} else if (author.getPosts() < 200) {
			statementOne = 0.6;
			statementTwo = 3;
		} else if (author.getPosts() < 300) {
			statementOne = 0.5;
			statementTwo = 3;
		} else if (author.getPosts() < 400) {
			statementOne = 0.5;
			statementTwo = 2;
		} else if (author.getPosts() < 500) {
			statementOne = 0.6;
			statementTwo = 2;
		}
		
		// Berechnung 1
		if (a > statementOne) {
			author.incrementPoints(50);
		}

		// Berechnung 2
		if (a/statementTwo > b) {
			author.incrementPoints(100);
		}
	}
}
