package ctrl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Author;
import model.Review;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class DbCtrl {
	private final String SSH_USER = "username";
	private final String SSH_PWD = "pwd";
	private final int SSH_PORT = 223;
	private final String PSQL_USER = "postgres";
	private final String PSQL_PWD = "iHC3QZ2zKQx1b2QW21hq";
	private final String IP = "jheym.de";
	
	private Connection connection = null;
	private Session session;
	
	private void connect() {
		if (connection == null) {
			try {
				// Tunnel aufspannen
				//this.tunnelConnect();
				
				// Datenbankverbindung herstellen
				Class.forName("org.postgresql.Driver");
				connection = DriverManager.getConnection("jdbc:postgresql://jheym.de:5432/datatextmining", PSQL_USER, PSQL_PWD);
				
				//result.setAutoCommit(false); //AutoCommit deaktivieren
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				if (connection.isClosed()) {
					connection = DriverManager.getConnection("jdbc:postgresql://jheym.de:5432/datatextmining", PSQL_USER, PSQL_PWD);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void tunnelConnect() {
		// Verbindung aufbauen
		JSch jsch = new JSch();
		
		try {
			this.session = jsch.getSession(SSH_USER, IP, SSH_PORT);
			this.session.setPassword(SSH_PWD);
			
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			this.session.setConfig(config);
			
			this.session.connect();
		} catch (JSchException e) {
			e.printStackTrace();
			return;
		}
		
		// Tunnel/PortForwarding
		try {
			// DestPort, Ip, SrcPort
			this.session.setPortForwardingL(5432, IP, 5432);
		} catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	// AUTHOR
	public ArrayList<Author> getAuthors() {
		ArrayList<Author> result = new ArrayList<Author>();
		Statement statement = null;
		String sql = "SELECT * FROM author";

		// Datenbankverbindung herstellen
		this.connect();

		try {
			// Daten abfragen
			statement = connection.createStatement();
			ResultSet sqlResults = statement.executeQuery(sql);

			// Daten verarbeiten
			while (sqlResults.next()) {
				result.add(createAuthorFromSqlResult(sqlResults));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public Author getAuthorById(int authorId) {
		Author result = null;
		Statement statement = null;
		String sql = "SELECT * FROM author WHERE author = " + authorId;

		// Datenbankverbindung herstellen
		this.connect();

		try {
			// Daten abfragen
			statement = connection.createStatement();
			ResultSet sqlResults = statement.executeQuery(sql);

			// Daten verarbeiten
			while (sqlResults.next()) {
				result = createAuthorFromSqlResult(sqlResults);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	// REVIEWS
	public ArrayList<Review> getReviews() {
		ArrayList<Review> result = new ArrayList<Review>();
		Statement statement = null;
		String sql = "SELECT * FROM daten";

		// Datenbankverbindung herstellen
		this.connect();

		try {
			// Daten abfragen
			statement = connection.createStatement();
			ResultSet sqlResults = statement.executeQuery(sql);

			// Daten verarbeiten
			while (sqlResults.next()) {
				result.add(createReviewFromSqlResult(sqlResults));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public ArrayList<Review> getReviewsByAuthor(int authorId) {
		ArrayList<Review> result = new ArrayList<Review>();
		Statement statement = null;
		String sql = "SELECT * FROM daten WHERE author = " + authorId;

		// Datenbankverbindung herstellen
		this.connect();

		try {
			// Daten abfragen
			statement = connection.createStatement();
			ResultSet sqlResults = statement.executeQuery(sql);

			// Daten verarbeiten
			while (sqlResults.next()) {
				result.add(createReviewFromSqlResult(sqlResults));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public ArrayList<Review> getReviewsByItem(int itemId) {
		ArrayList<Review> result = new ArrayList<Review>();
		Statement statement = null;
		String sql = "SELECT * FROM daten WHERE item = " + itemId;

		// Datenbankverbindung herstellen
		this.connect();

		try {
			// Daten abfragen
			statement = connection.createStatement();
			ResultSet sqlResults = statement.executeQuery(sql);

			// Daten verarbeiten
			while (sqlResults.next()) {
				result.add(createReviewFromSqlResult(sqlResults));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	// ITEMS
	public ArrayList<Integer> getItemIds() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		Statement statement = null;
		String sql = "SELECT DISTINCT item FROM daten";

		// Datenbankverbindung herstellen
		this.connect();

		try {
			// Daten abfragen
			statement = connection.createStatement();
			ResultSet sqlResults = statement.executeQuery(sql);

			// Daten verarbeiten
			while (sqlResults.next()) {
				result.add(sqlResults.getInt("item"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	// SONSTIGES
 	public Author getAuthorWithReviewsByAuthor(int authorId) {
		Author author = null;
		Statement statement = null;
		String sql = "SELECT * FROM author WHERE id = " + authorId;

		// Datenbankverbindung herstellen
		this.connect();

		try {
			// Daten abfragen
			statement = connection.createStatement();
			ResultSet sqlResults = statement.executeQuery(sql);

			// Daten verarbeiten
			while (sqlResults.next()) {
				author = createAuthorFromSqlResult(sqlResults);
				author.setReviews(this.getReviewsByAuthor(authorId));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return author;
	}
	
	// CREATE HILFSFUNKTIONEN
 	private Author createAuthorFromSqlResult(ResultSet sqlResult) {
		Author newAuthor = null;

		try {
			newAuthor = new Author(sqlResult.getInt("id"), 
					sqlResult.getInt("author"),
					sqlResult.getDouble("avgrating"),
					sqlResult.getInt("posts"),
					sqlResult.getInt("points")
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return newAuthor;
	}
	
	private Review createReviewFromSqlResult(ResultSet sqlResult) {
		Review newReview = null;

		try {
			newReview = new Review(sqlResult.getInt("id"), 
					sqlResult.getString("title"),
					sqlResult.getString("text"),
					sqlResult.getInt("rating"),
					sqlResult.getBoolean("verified"),
					sqlResult.getInt("counter_helpfull"),
					sqlResult.getInt("counter_all"),
					sqlResult.getString("author"),
					sqlResult.getString("date"),
					sqlResult.getInt("item"),
					sqlResult.getString("categorie")
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return newReview;
	}
}
