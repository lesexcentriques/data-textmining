package ctrl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.Author;
import model.ItemDummy;
import model.Review;

/*
 * Man betrachtet die mittleren vergebenen Sterne für einen Artikel und berechnet
 * die Standardabweichung. Mit Hilfe dieser Daten werden Bewertungen gefunden,
 * die besonders stark davon abweichen. Fällt hier ein Nutzer bei mehreren Artikeln
 * auf, ist das verdächtig.
 */

public class StandardDeviation {
	private final double PROPORTION_PERCENTAGE = 0.2;
	
	private DbCtrl dbCtrl;
	private ArrayList<Integer> itemIds;
	private ArrayList<ItemDummy> itemDummys = new ArrayList<ItemDummy>();
	private Map<String, Integer> resultMap = new HashMap<String, Integer>();
	
	public StandardDeviation(DbCtrl dbCtrl) {
		this.dbCtrl = dbCtrl;
	}
	
	public void doIt() {
		ItemDummy itemDummy = null;
		itemIds = dbCtrl.getItemIds();
		
		// Auswertungen pro Dummy
		int breakCnt = 0;
		int itemsAll = 0;
		for (int itemId : itemIds) {
			itemDummy = new ItemDummy(itemId, dbCtrl.getReviewsByItem(itemId));
			itemDummys.add(itemDummy);
			
			this.calcAvg(itemDummy);
			this.calcVariance(itemDummy);
			this.compareReviewsWithStandardDeviation(itemDummy);
			
			++breakCnt;
			if (breakCnt == 100) {
				itemsAll += 100;
				System.out.println(itemsAll + " items done.");
				breakCnt = 0;
				//break;
			}
		}
		System.out.println("All items prepared.");
		
		// Ergebnisse in Verhältnis setzen
		this.putResultsInRelation();
 	}
	
	private void calcAvg(ItemDummy itemDummy) {
		int cnt = 0;
		double sum = 0.0;
		
		for (Review review : itemDummy.getReviews()) {
			++cnt;
			sum += review.getRating();
		}
		
		itemDummy.setAvgRating(sum/cnt);
	}
	
	private void calcVariance(ItemDummy itemDummy) {
		double numerator = 0.0; // Zähler
		int denominator = 0; // Nenner
		double avgRating = itemDummy.getAvgRating();
		
		for (Review review : itemDummy.getReviews()) {
			++denominator;
			numerator += Math.pow(review.getRating()-avgRating, 2.0);
		}
		
		itemDummy.setVariance(numerator/denominator);
	}
	
	private void compareReviewsWithStandardDeviation(ItemDummy itemDummy) {
		String currAuthor;
		double benignRatingsTop = itemDummy.getAvgRating() + itemDummy.getStandardDeviation(); 
		double benignRatingsBottom = itemDummy.getAvgRating() - itemDummy.getStandardDeviation();
		
		for (Review review : itemDummy.getReviews()) {
			currAuthor = review.getAuthor();
			
			if (benignRatingsTop < 5 && review.getRating() > benignRatingsTop
					|| benignRatingsBottom > 0 && review.getRating() < benignRatingsBottom) {
				if (!resultMap.containsKey(currAuthor)) {
					resultMap.put(currAuthor, 1);
				} else {
					resultMap.put(currAuthor, resultMap.get(currAuthor) + 1);
				}
			}
		}
	}
	
	private void putResultsInRelation() {
		int cnt = 0;
		int value;
		Author currAuthor;
		double proportion;
		
		for (String key : resultMap.keySet()) {
			currAuthor = dbCtrl.getAuthorById(Integer.parseInt(key));
			value = resultMap.get(key);
			proportion = currAuthor.getPosts() * PROPORTION_PERCENTAGE;
			//System.out.println(proportion);
			
			if (proportion > 1.0 && value > proportion) {
				++cnt;
				System.out.println("Author: " + currAuthor.getAuthorId()
						+ ", Posts: " + currAuthor.getPosts()
						+ ", Value: " + value
						+ ", Proportion: " + proportion);
			}
		}
		
		System.out.println(cnt + " authors notable.");
	}
}
