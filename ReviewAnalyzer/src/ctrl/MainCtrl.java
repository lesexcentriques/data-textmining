package ctrl;

import java.util.ArrayList;

import model.Author;
import model.Review;

public class MainCtrl {

	public static void main(String[] args) {
		DbCtrl dbCtrl = new DbCtrl();
		
//		HighLowRating hlRating = new HighLowRating(dbCtrl, 20);
//		hlRating.doIt();
		
		StandardDeviation stdDeviation = new StandardDeviation(dbCtrl);
		stdDeviation.doIt();
		
//		ArrayList<Review> reviews = dbCtrl.getReviewsByAuthor(46780);
//		for (Review review : reviews) {
//			System.out.println(review);
//		}
		
//		Author author = dbCtrl.getAuthorWithReviewsByAuthor(46780);
//		System.out.println(author);
		
//		ArrayList<Author> authors = dbCtrl.getAuthors();
//		for (Author author : authors) {
//			System.out.println(author);
//		}
	}

}
